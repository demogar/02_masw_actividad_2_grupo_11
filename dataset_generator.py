import json
import random
import datetime

nombres = [
    "Juan",
    "María",
    "Pedro",
    "Laura",
    "Carlos",
    "Ana",
    "Miguel",
    "Isabel",
    "Luis",
    "Elena"
]

apellidos = [
    "García",
    "Martínez",
    "López",
    "Hernández",
    "González",
    "Rodríguez",
    "Pérez",
    "Sánchez",
    "Romero",
    "Torres"
]

ciudades = {
    "Quito": {"lat": -0.2295, "lon": -78.5249},
    "Guayaquil": {"lat": -2.1709, "lon": -79.9224},
    "Cuenca": {"lat": -2.9006, "lon": -79.0059},
    "Ambato": {"lat": -1.2491, "lon": -78.6199},
    "Ibarra": {"lat": 0.3476, "lon": -78.1223},
    "Loja": {"lat": -3.9931, "lon": -79.2042},
    "Latacunga": {"lat": -0.9340, "lon": -78.6157},
    "Tena": {"lat": -0.9936, "lon": -77.8109}
}

def generate_dataset():
    dataset = {"ventas": []}
    stores = generate_stores(10)
    products, categories = generate_products(150)
    buyers = generate_buyers(80)

    for _ in range(100000):
        fecha_venta = random_date()
        tienda = random.choice(stores)
        productos = generate_sale_products(products, categories)
        comprador = random.choice(buyers)

        # Calcular el total de la venta, impuestos y descuentos
        total = 0
        impuestos = 0
        descuentos = 0

        for producto in productos:
            total += producto["precio_total"]
            impuestos += producto["impuestos"]
            descuentos += producto["descuento"]

        venta = {
            "fecha_venta": fecha_venta.strftime("%Y-%m-%d %H:%M:%S"),
            "tienda": {
                "id": tienda["id"],
                "nombre": tienda["nombre"],
                "ubicacion": tienda["ubicacion"]
            },
            "total": round(total, 2),
            "impuestos": round(impuestos, 2),
            "descuentos": round(descuentos, 2),
            "productos": productos,
            "comprador": {
                "id": comprador["id"],
                "nombre": comprador["nombre"],
                "telefono": comprador["telefono"],
                "correo": comprador["correo"]
            }
        }

        dataset["ventas"].append(venta)

    return dataset

def random_date():
    # Vamos a generar todo durante el 2022
    start_date = datetime.datetime(2022, 1, 1)
    end_date = datetime.datetime(2022, 12, 31)
    return start_date + datetime.timedelta(seconds=random.randint(0, int((end_date - start_date).total_seconds())))

def generate_stores(num_stores):
    stores = []

    for i in range(1, num_stores+1):
        ciudad = random.choice(list(ciudades.keys()))
        store = {
            "id": i,
            "nombre": ciudad,
            "ubicacion": {
                "lat": ciudades[ciudad]["lat"],
                "lon": ciudades[ciudad]["lon"]
            }
        }

        stores.append(store)

    return stores

def generate_products(num_products):
    products = []
    categories = [
        "asadores electricos",
        "asadores de gas",
        "asadores de pellets",
        "asadores portatiles",
        "accesorios"
    ]

    product_names = {
        "asadores electricos": ["Asador 300E", "Asador 500E", "Asador 700E"],
        "asadores de gas": ["Asador 100G", "Asador 200G"],
        "asadores de pellets": ["Asador 5P", "Asador 7P", "Asador 9P"],
        "asadores portatiles": ["Mini-Asador P1", "Mini-Asador P2"],
        "accesorios": [
            "Prensa Hamburguesa",
            "Plancha Pizza",
            "Carrito de Asar",
            "Espátula flexible",
            "Raspador",
            "Kit Especial 1",
            "Kit Especial 2",
            "Kit Especial 3",
            "Tabla para picar 1",
            "Tabla para picar 2",
            "Tabla para picar 3",
            "Tabla para picar 4"
        ]
    }

    for i in range(1, num_products + 1):
        category = random.choice(categories)
        product_name = random.choice(product_names[category])
        precio_original = round(random.uniform(1, 100), 2)
        descuento = round(random.uniform(0, 0.05 * precio_original), 2)
        impuestos = round(0.1 * (precio_original - descuento), 2)
        precio = round(precio_original - descuento + impuestos, 2)
        product = {
            "id": i,
            "nombre": product_name,
            "precio": precio,
            "descuento": descuento,
            "impuestos": impuestos,
            "categoria": category
        }

        products.append(product)

    return products, categories

def generate_buyers(num_buyers):
    buyers = []

    for i in range(1, num_buyers+1):
        nombre = random.choice(nombres)
        apellido = random.choice(apellidos)
        buyer = {
            "id": i,
            "nombre": f"{nombre} {apellido}",
            "telefono": f"555-123-{random.randint(1000, 9999)}",
            "correo": f"comprador{i}@example.com"
        }

        buyers.append(buyer)

    return buyers

def generate_sale_products(products, categories):
    sale_products = []

    num_products = random.randint(1, 5)
    product_ids = random.sample(range(1, len(products) + 1), num_products)

    for product_id in product_ids:
        product = products[product_id - 1]
        precio_original = product["precio"]
        descuento = product["descuento"]
        impuestos = round(0.1 * (precio_original - descuento), 2)
        precio_total = round(precio_original + impuestos - descuento, 2)

        sale_product = {
            "id": product_id,
            "nombre": product["nombre"],
            "precio": precio_original,
            "descuento": descuento,
            "impuestos": impuestos,
            "precio_total": precio_total,
            "categoria": product["categoria"]
        }

        sale_products.append(sale_product)

    return sale_products

# Generar el dataset ficticio
dataset = generate_dataset()

# Guardar el dataset en un archivo JSON
with open("dataset.json", "w") as file:
    json.dump(dataset, file, indent=2, ensure_ascii=False)

print("Dataset ficticio generado y guardado en 'dataset.json'")
